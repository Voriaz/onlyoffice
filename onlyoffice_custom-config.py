import json
import os

try:
    secret_key = os.environ['onlyoffice_secretkey']

    configfile = open("/etc/onlyoffice/documentserver/local.json","r") 

    config = json.loads(configfile.read())
    config["services"]["CoAuthoring"]["secret"]["inbox"]["string"] = secret_key
    config["services"]["CoAuthoring"]["secret"]["outbox"]["string"] = secret_key
    config["services"]["CoAuthoring"]["secret"]["session"]["string"] = secret_key
    config["services"]["CoAuthoring"]["token"]["enable"]["request"]["inbox"] = True
    config["services"]["CoAuthoring"]["token"]["enable"]["request"]["outbox"] = True
    config["services"]["CoAuthoring"]["token"]["enable"]["browser"] = True

    configfile.close() 

    configfile = open("/etc/onlyoffice/documentserver/local.json","w") 

    configfile.write(json.dumps(config))
    configfile.close()

except KeyError:
    print("Environement Variable 'onlyoffice_secretkey' does not exists.\nSkipping and using defaults")
