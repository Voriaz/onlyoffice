FROM onlyoffice/documentserver

COPY onlyoffice_custom-config.py /etc/onlyoffice/onlyoffice_custom-config.py

COPY onlyoffice_custom-config.sh /etc/onlyoffice/onlyoffice_custom-config.sh

RUN chmod +x /etc/onlyoffice/onlyoffice_custom-config.sh

ENTRYPOINT /etc/onlyoffice/onlyoffice_custom-config.sh
