# How to use ?

* Clone this repo
* cd into the repo's folder
* Type the following command and replace **<your_secret_key_goes_here>** by the secret key you want:

```bash
docker build -t voriaz/onlyoffice .
sudo docker run -it -d -p 80:80 --name="onlyoffice" --restart=always -e onlyoffice_secretkey="<your_secret_key_goes_here>" voriaz/onlyoffice
```

* Enjoy
